Reglas de convivencia
---------------------

1.- Área de trabajo limpia y ordenada.

2.- Apagar la luz si no se está usando.

3.- Apagar la luz del baño y cerrar la puerta al salir.

4.- Beber agua en vasos de vidrio.

5.- Limpiar la cocina al terminar de usarla.

6.- Ordenar los utensilios de cocina usados.

7.- No dejar en el refrigerador comida en mal estado.

8.- Si los cestos de basura están llenos tirar la basura.

9.- Mantener los cuartos limpios y ordenados.

10.- Sacar la basura orgánica al final del dia.

11.- No fumar dentro de la oficina.

